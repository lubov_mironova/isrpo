import java.util.Scanner;

class Main {
    public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int r;
        int max = 0;

        System.out.print("Введите количество чисел: ");
        int n = scan.nextInt();

        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Введите число: ");
            a[i] = scan.nextInt();
            max = -1;
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] + a[j] > max && (a[i] + a[j]) % 2 != 0) {
                    max = a[i] + a[j];
                }
            }
        }

        System.out.print("Введите контрольное значение: ");
        r = scan.nextInt();

        System.out.println("Вычисленное контрольное значение: " + max);

        if (r != max) {
            System.out.println("Контроль не пройден");
        } else {
            System.out.println("Контроль пройден");
        }
    }
}