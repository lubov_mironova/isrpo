package testing;

import java.io.FileInputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

class TestingMain {
    public static Scanner scan = new Scanner(System.in);
    private static Logger logger;

    static {
        try (FileInputStream configFile = new FileInputStream("src\\logging\\logging.config")) {
            LogManager.getLogManager().readConfiguration(configFile);
            logger = Logger.getLogger(TestingMain.class.getName());
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    public static void main(String[] args) {
        logger.log(Level.INFO, "Начало программы, присваивание значении для переменных");
        int max = 0;

        System.out.print("Введите количество чисел: ");
        int n = scan.nextInt();

        int[] a = new int[n];

        logger.log(Level.INFO, "Вход в цикл программы");
        for (int i = 0; i < n; i++) {
            System.out.print("Введите число: ");
            a[i] = scan.nextInt();
            max = -1;
        }
        logger.log(Level.INFO, "Вход в цикл программы");
        int result = test(n,a, max);

        System.out.print("Введите контрольное значение: ");
        int r = scan.nextInt();

        System.out.println("Вычисленное контрольное значение: " + result);

        logger.log(Level.INFO, "Проверка условия { r != max } ");
        if (r != max) {
            System.out.println("Контроль не пройден");
        } else {
            System.out.println("Контроль пройден");
        }

        logger.log(Level.INFO, "Конец программы");
    }

    public static int test(int n, int[] a, int max) {
        for (int i = 0; i < n - 1; i++) {
            logger.log(Level.INFO, "Вход в цикл программы");
            for (int j = i + 1; j < n; j++) {
                logger.log(Level.INFO, "Проверка условия { a[i] + a[j] > max && (a[i] + a[j]) % 2 != 0 } ");
                if (a[i] + a[j] > max && (a[i] + a[j]) % 2 != 0) {
                    max = a[i] + a[j];
                }
            }
        }
        return max;
    }
}