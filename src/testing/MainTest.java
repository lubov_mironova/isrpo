package testing;

import org.junit.Assert;

public class MainTest {

    @org.junit.jupiter.api.Test
    void main() {
        int[] a = new int[]{8, 100, 33, 19, 45, 28, 90, 145};
        int n = 8;
        int max = -1;
        int test = TestingMain.test(n, a, max);
        int actual = 245;
        Assert.assertEquals(test, actual);
    }
}
